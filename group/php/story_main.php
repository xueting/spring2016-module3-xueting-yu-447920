<?php
/**
 * HTML codes are based on
 * http://blackrockdigital.github.io/startbootstrap-blog-home/
 * with custom modification
 */

include 'util.php';
require 'database.php';

session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Module 3-Xueting&Shitianyu</title>

    <!-- Bootstrap Core CSS -->
    <link href="../bootstrap-3.3.6-dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../css/story_main.css" rel="stylesheet">
</head>

<body>

<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">HOME</a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li>
                    <a href="#">About</a>
                </li>
                <li>
                    <a href="#">Contact</a>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <?php
                if (is_null(SESSION('user_name'))) {
                    echo "<li><a href='login.php'>Sign in</a></li>";
                } else {
                    echo "<li><a href='#'>Hello, " . SESSION('user_name') . "</a></li>
                    <li><a href='logout.php'>Sign out</a></li>";
                }
                ?>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>

<!-- Page Content -->
<div class="container">

    <div class="row">

        <!-- Blog Entries Column -->
        <div class="col-md-8">

            <h1 class="page-header">
                Simple News Web Site
                <small>Module3</small>
            </h1>

            <?php
            //search by author
            $author = GET('author');
            $tag = GET('tag');
            $sql = "";
            if (!is_null($author)) {
                $sql = " WHERE author=? ";
            }

            //search by tag
            if ($tag == "art") {
                $sql = " WHERE tag_art=1";
            } else if ($tag == "sport") {
                $sql = " WHERE tag_sport=1";
            } else if ($tag == "tech") {
                $sql = " WHERE tag_technology=1";
            } else if ($tag == "other") {
                $sql = " WHERE tag_other=1";
            }

            //create paging
            $page = GET('page');
            $stmt = $mysqli->prepare("SELECT count(*) FROM STORIES_LINK" . $sql);
            if (!$stmt) {
                printf("Query Prep Failed: %s\n", $mysqli->error);
                exit;
            }
            if (!is_null($author)) {
                $stmt->bind_param("s", $author);
            }

            $stmt->execute();

            // Bind the results
            $stmt->bind_result($cnt);
            $stmt->fetch();
            $stmt->close();

            $offset = 0;
            $page_limit = 3;

            if (!is_null($page)) {
                $page++;
                $offset = $page_limit * $page;
            }

            $stmt = $mysqli->prepare("SELECT linkID, author, story_title, story_brief, image_path, tag_other, tag_art, tag_sport, tag_technology
FROM STORIES_LINK" . $sql . " LIMIT ?, ?");
            if (!$stmt) {
                printf("Query Prep Failed: %s\n", $mysqli->error);
                exit;
            }
            if (!is_null($author)) {
                $stmt->bind_param('sii', $author, $offset, $page_limit);
            } else {
                $stmt->bind_param('ii', $offset, $page_limit);
            }
            $stmt->execute();
            $stmt->bind_result($linkID, $username, $story_title, $story_brief, $image_path, $tag_other, $tag_art, $tag_sport, $tag_technology);
            //blog post
            while ($stmt->fetch()) {
                if (!is_null($image_path)) {
                    $image_path = "/home/doublefinger/uploads/module3_images/" . $image_path;
                    $image_data = base64_encode(file_get_contents($image_path));
                    $src = 'data: ' . mime_content_type($image_path) . ';base64,' . $image_data;
                    $image = '<img style="width:900px;height:300px;" class="img-responsive" src="' . $src . '" alt="' . $image_path . '"><hr>';
                } else {
                    $image = "";
                }

                $tag = "";
                if ($tag_other == 1) {
                    $tag .= '<span id="dada" class="label label-default">other</span>';
                }
                if ($tag_art == 1) {
                    $tag .= '<span class="label label-danger">art</span>';
                }
                if ($tag_sport == 1) {
                    $tag .= '<span class="label label-warning">sport</span>';
                }
                if ($tag_technology == 1) {
                    $tag .= '<span class="label label-info">technology</span>';
                }

                echo '<h2><a href="#">' . $story_title . '</a></h2> ' . $tag . '
            <p class="lead">
                by <a href="#">' . $username . '</a>
            </p>
            <hr>' . $image . '
            <p>' . $story_brief . '</p>
            <a class="btn btn-primary" href="story_view.php/' . $linkID . '">Read More <span class="glyphicon glyphicon-chevron-right"></span></a>
            <hr>';
            }

            $stmt->close();
            ?>

            <!-- Pager -->
            <ul class="pager">
                <li class="previous">
                    <?php
                    $prev_page = $page - 1;
                    if ($prev_page < 0)
                        $prev_page = 0;
                    echo '<a href="story_main.php?page=' . $prev_page . '">&larr; Older</a>'
                    ?>
                </li>
                <li class="next">
                    <?php
                    $next_page = $page + 1;
                    if ($next_page > $cnt - $page_limit)
                        $next_page = $cnt - $page_limit;
                    echo '<a href="story_main.php?page=' . $next_page . '">Newer &rarr;</a>'
                    ?>
                </li>
            </ul>

        </div>

        <!-- Blog Sidebar Widgets Column -->
        <div class="col-md-4">

            <!-- Blog Search Well -->
            <div class="well">
                <h4>Search by Author</h4>
                <form action="search.php">
                    <div class="input-group">
                        <input type="text" name="author" class="form-control">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="submit">
                                <span class="glyphicon glyphicon-search"></span>
                            </button>
                        </span>

                    </div>
                </form>
                <!-- /.input-group -->
            </div>

            <!-- Blog Categories Well -->
            <div class="well">
                <h4>Categories</h4>
                <div class="row">
                    <div class="col-lg-2">
                        <ul class="list-unstyled">
                            <li><a href="story_main.php?tag=art">Art</a>
                            </li>
                            <li><a href="story_main.php?tag=sport">Sport</a>
                            </li>
                            <li><a href="story_main.php?tag=tech">Technology</a>
                            </li>
                            <li><a href="story_main.php?tag=other">Others</a>
                            </li>
                        </ul>
                    </div>
                    <!-- /.col-lg-6 -->
                </div>
                <!-- /.row -->
            </div>

            <!-- Side Widget Well -->
            <div class="well">
                <h4><a href="story_post.php">POST YOUR OWN STORY</a></h4>
                <p>You are welcome to share your story and other interesting information through our website. To do
                    this, you will need to register as a member of our website. It is FREE!</p>
            </div>

        </div>

    </div>
    <!-- /.row -->

    <hr>

    <!-- Footer -->
    <footer>
        <div class="row">
            <div class="col-lg-12">
                <p>Copyright &copy; Shitianyu Pan, Xueting Yu</p>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </footer>

</div>
<!-- /.container -->

<!-- jQuery -->
<script src="../jquery/jquery-1.12.0.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="../bootstrap-3.3.6-dist/js/bootstrap.min.js"></script>

</body>

</html>