<?php
/**
 * edit based on wiki
 */
include 'util.php';
require 'database.php';

session_start();
$id = POST('linkID');
$username = SESSION('user_name');
$story_title = htmlentities(trim(POST('story_title')));
$story_body = htmlentities(trim(POST('story_body')));
$tag_other = POST('other');
$tag_art = POST('art');
$tag_tech = POST('tech');
$tag_sport = POST('sport');

$tag_other = is_null($tag_other) ? 0 : 1;
$tag_art = is_null($tag_art) ? 0 : 1;
$tag_tech = is_null($tag_tech) ? 0 : 1;
$tag_sport = is_null($tag_sport) ? 0 : 1;

//upload image
$full_name = $_FILES['image']['name'];
$filename = htmlentities(basename($full_name));
if (!validateFileName($filename)) {
    echo "Invalid Filename";
    exit;
}
$ext = pathinfo($full_name, PATHINFO_EXTENSION);

if (!validateExtension($ext)) {
    echo "Invalid Image Format";
    exit;
}

$full_path = sprintf("/home/doublefinger/uploads/module3_images/%s", $filename);

$story_brief = $story_body;
$body_string = (string)$story_body;
//get first 100 chars as the story brief
if (strlen($body_string) > 600)
    $story_brief = substr($body_string, 0, 600);

//post story
if (is_null($id)) {
    //insert the story info into table STORIES_LINK
    $stmt = $mysqli->prepare("INSERT INTO STORIES_LINK (author, story_title, story_brief, image_path, tag_other, tag_art, tag_sport, tag_technology) VALUES (?, ?, ?, ?, ?, ?, ?, ?)");
    if (!$stmt) {
        printf("Query Prep Failed: %s\n", $mysqli->error);
        exit;
    }
    $stmt->bind_param('ssssiiii', $username, $story_title, $story_brief, $filename, $tag_other, $tag_art, $tag_sport, $tag_tech);
    $stmt->execute();

    $linkID = $mysqli->insert_id;
    $stmt->close();

    //insert the story body into table STORIES

    $stmt = $mysqli->prepare("INSERT INTO STORIES(storyID, story_body, created_at, updated_at) VALUES (?, ?, now(), now())");
    if (!$stmt) {
        printf("Query Prep Failed: %s\n", $mysqli->error);
        exit;
    }
    $stmt->bind_param('is', $linkID, $story_body);
    $stmt->execute();
    $stmt->close();

    //upload the image to the file system, do not overwrite
    if (is_file($full_path)) {
        $temp = pathinfo($filename, PATHINFO_FILENAME);
        $temp .= "1";
        $full_path = sprintf("/home/doublefinger/uploads/module3_images/%s", $temp . "." . $ext);
    }



} else {
    $stmt = $mysqli->prepare("UPDATE STORIES_LINK SET story_title = ?, story_brief = ?, image_path = ?, tag_other = ?, tag_art=?, tag_sport=?, tag_technology=? WHERE linkID = ?");
    if (!$stmt) {
        printf("Query Prep Failed: %s\n", $mysqli->error);
        exit;
    }
    $stmt->bind_param('sssiiiii', $story_title, $story_brief, $filename, $tag_other, $tag_art, $tag_sport, $tag_tech, $id);
    $stmt->execute();
    $stmt->close();

    $stmt = $mysqli->prepare("UPDATE STORIES SET story_body = ?, updated_at = now()  WHERE storyID =?");
    if (!$stmt) {
        printf("Query Prep Failed: %s\n", $mysqli->error);
        exit;
    }
    $stmt->bind_param('si', $story_body, $id);
    $stmt->execute();
    $stmt->close();

    //upload the image to the file system, overwrite
    if (is_file($full_path)) {
        $temp = pathinfo($filename, PATHINFO_FILENAME);
        $temp .= "1";
        $full_path = sprintf("/home/doublefinger/uploads/module3_images/%s", $temp . "." . $ext);
    }
}

if (!move_uploaded_file($_FILES['image']['tmp_name'], $full_path)) {
    echo "Image_upload_fail";
    exit;
}
header("Location: story_main.php");
exit;
