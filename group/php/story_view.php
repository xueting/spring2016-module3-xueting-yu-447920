<?php
/**
 * HTML codes are based on
 * http://blackrockdigital.github.io/startbootstrap-blog-post/
 * with custom modification
 */
include 'util.php';
require 'database.php';

session_start();
$username = SESSION('user_name');
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">

    <title>Module 3-Xueting&Shitianyu</title>

    <!-- Bootstrap Core CSS -->
    <link href="../../bootstrap-3.3.6-dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../css/story_view.css" rel="stylesheet">
</head>

<body>

<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">HOME</a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li>
                    <a href="#">About</a>
                </li>
                <li>
                    <a href="#">Contact</a>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <?php
                if (is_null(SESSION('user_name'))) {
                    echo "<li><a href='login.php'>Sign in</a></li>";
                } else {
                    echo "<li><a href='#'>Hello, " . SESSION('user_name') . "</a></li>
                    <li><a href='logout.php'>Sign out</a></li>";
                }
                ?>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>

<!-- Page Content -->
<div class="container">

    <div class="row">

        <!-- Blog Post Content Column -->
        <div class="col-lg-8">
            <?php
            $request = $_SERVER['REQUEST_URI'];

            $id = substr($request, strrpos($request, '/') + 1);
            if (!is_numeric($id)) {
                header("Location: ../story_main.php");
                exit;
            }
            $stmt = $mysqli->prepare("SELECT count(*), story_body, created_at, updated_at FROM STORIES WHERE storyID=?");
            if (!$stmt) {
                printf("Query Prep Failed: %s\n", $mysqli->error);
                exit;
            }
            $stmt->bind_param('i', $id);
            $stmt->execute();
            $stmt->bind_result($cnt, $story_body, $created_at, $updated_at);
            $stmt->fetch();
            if ($cnt <= 0) {
                header("Location: ../story_main.php");
                exit;
            }
            $stmt->close();

            $stmt = $mysqli->prepare("SELECT author, story_title, image_path FROM STORIES_LINK WHERE linkID=?");
            if (!$stmt) {
                printf("Query Prep Failed: %s\n", $mysqli->error);
                exit;
            }
            $stmt->bind_param('i', $id);
            $stmt->execute();
            $stmt->bind_result($author, $story_title, $image_path);
            $stmt->fetch();
            $stmt->close();

            if (!is_null($image_path)) {
                $image_path = "/home/doublefinger/uploads/module3_images/" . $image_path;
                $image_data = base64_encode(file_get_contents($image_path));
                $src = 'data: ' . mime_content_type($image_path) . ';base64,' . $image_data;
                $image = '<img style="width:900px;height:300px;" class="img-responsive" src="' . $src . '" alt="' . $image_path . '"><hr>';
            } else {
                $image = "";
            }

            echo '<h1>' . $story_title . '</h1>
            <!-- Author -->
            <p class="lead">by '.$author.' <a href="#"></a></p><hr>

            <!-- Date/Time -->
            <p><span class="glyphicon glyphicon-time"></span> Posted on ' . $created_at . '</p>
            <hr>
            <!-- Preview Image -->
            ' . $image . '
            <p>' . $story_body . '</p>' . $action . '
            <hr>';
            ?>
            <!-- Comments Form -->
            <div class="well">
                <h4>Leave a Comment:</h4>
                <form action="../comment_post.php" method="POST">
                    <input type="text" name="storyID" hidden="hidden" value="<?php
                    echo $id;
                    ?>">
                    <input type="text" name="author" hidden="hidden" value="<?php
                    echo $username;
                    ?>">
                    <div class="form-group">
                        <textarea class="form-control" rows="3" name="comment"></textarea>
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>

            <hr>

            <!-- Posted Comments -->

            <!-- Comment -->
            <div class="media">
                <div class="media-body">
                    <?php
                    $stmt = $mysqli->prepare("SELECT commentID, author, comment_body, created_at FROM COMMENTS WHERE storyID = ?");
                    if (!$stmt) {
                        printf("Query Prep Failed: %s\n", $mysqli->error);
                        exit;
                    }
                    $stmt->bind_param("i", $id);
                    $stmt->execute();
                    $stmt->bind_result($commentID, $comment_author, $comment_body, $comment_date);
                    while ($stmt->fetch()) {
                        echo '<h4 class="media-heading">' . $comment_author . '
                            <small>' . $comment_date . ' </small>
                            <a class="btn btn-xs btn-danger" href="../comment_delete.php?comment_author='.$comment_author.'&commentID='.$commentID.'">Delete</a>
                            <a class="btn btn-xs btn-default" href="../comment_edit.php?comment_author='.$comment_author.'&commentID='.$commentID.'">Edit</a>
                            </h4>' . $comment_body;
                    }
                    ?>
                </div>
            </div>
        </div>

        <!-- Blog Sidebar Widgets Column -->
        <div class="col-md-4">
            <div class="well">
                <h4><a href="../story_post.php">POST YOUR OWN STORY</a></h4>
                <p>You are welcome to share your story and other interesting information through our website. To do
                    this, you will need to register as a member of our website. It is FREE!</p>
            </div>
            <?php
            if ($_SESSION['user_name'] == $author) {
                echo '<form action="../story_delete.php" method="post">
                <input type="text" name="author" hidden="hidden" value="' . $author . '">
                <input type="text" name="storyID" hidden="hidden" value="' . $id . '">
                <button style="width:150px" class="btn btn-danger" type="submit">Delete Current Post</button>
            </form><br><form action="../story_edit.php" method="post">
                <input type="text" name="author" hidden="hidden" value="' . $author . '">
                <input type="text" name="storyID" hidden="hidden" value="' . $id . '">
                <button style="width:150px" class="btn btn-default" type="submit">Edit Current Post</button>
            </form>';
            }
            ?>

        </div>

    </div>
    <!-- /.row -->

    <hr>

    <!-- Footer -->
    <footer>
        <div class="row">
            <div class="col-lg-12">
                <p>Copyright &copy; Your Website 2014</p>
            </div>
        </div>
        <!-- /.row -->
    </footer>

</div>
<!-- /.container -->

<!-- jQuery -->
<script src="../../jquery/jquery-1.12.0.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="../../bootstrap-3.3.6-dist/js/bootstrap.min.js"></script>

</body>

</html>
