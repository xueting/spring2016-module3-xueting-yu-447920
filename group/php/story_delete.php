<?php
/**
 * Created by PhpStorm.
 * User: Doublefinger
 * Date: 2/22/16
 * Time: 8:21 AM
 */

include 'util.php';
require 'database.php';
session_start();
$author = POST("author");
$username = SESSION('user_name');
if (is_null($username) || $username != $author) {
    header("Location: story_main.php");
    exit;
}
$id = POST("storyID");
$stmt = $mysqli->prepare("DELETE FROM STORIES WHERE storyID=?");
if(!$stmt){
    printf("Query Prep Failed: %s\n", $mysqli->error);
    exit;
}
$stmt->bind_param('i', $id);
$stmt->execute();
$stmt->close();

$stmt = $mysqli->prepare("DELETE FROM STORIES_LINK WHERE linkID=?");
if(!$stmt){
    printf("Query Prep Failed: %s\n", $mysqli->error);
    exit;
}
$stmt->bind_param('i', $id);
$stmt->execute();
$stmt->close();


$stmt = $mysqli->prepare("DELETE FROM COMMENTS WHERE storyID=?");
if(!$stmt){
    printf("Query Prep Failed: %s\n", $mysqli->error);
    exit;
}
$stmt->bind_param('i', $id);
$stmt->execute();
$stmt->close();

header("Location: story_main.php");
exit;