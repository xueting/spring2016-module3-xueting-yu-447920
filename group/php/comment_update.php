<?php
/**
 * Created by PhpStorm.
 * User: Doublefinger
 * Date: 2/22/16
 * Time: 12:34 PM
 */
include 'util.php';
require 'database.php';
session_start();
$author = POST("author");
$id = POST('commentID');
$comment_body = POST('comment');

$username = SESSION('user_name');
if (is_null($username) || $username != $author) {
    header("Location: story_main.php");
    exit;
}

$stmt = $mysqli->prepare("UPDATE COMMENTS SET comment_body = ? WHERE commentID = ?");
if (!$stmt) {
    printf("Query Prep Failed: %s\n", $mysqli->error);
    exit;
}
$stmt->bind_param("si", $comment_body, $id);
$stmt->execute();
$stmt->close();
header("Location: story_main.php");
exit;
