<?php
/**
 * Created by PhpStorm.
 * User: Doublefinger
 * Date: 2/21/16
 * Time: 1:45 PM
 */
include 'util.php';
include 'database.php';

$mysqli = new mysqli('localhost', 'visitor', 'module3', 'mod3_group');

if($mysqli->connect_errno) {
    printf("Connection Failed: %s\n", $mysqli->connect_error);
    exit;
}