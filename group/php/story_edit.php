<?php
/**
 * Created by PhpStorm.
 * User: Doublefinger
 * Date: 2/22/16
 * Time: 9:37 AM
 */
include 'util.php';
require 'database.php';
session_start();
$author = POST("author");
$username = SESSION('user_name');
if (is_null($username) || $username != $author) {
    header("Location: story_main.php");
    exit;
}
$id = POST("storyID");
$stmt = $mysqli->prepare("SELECT story_body FROM STORIES WHERE storyID=?");
if(!$stmt){
    printf("Query Prep Failed: %s\n", $mysqli->error);
    exit;
}
$stmt->bind_param('i', $id);
$stmt->execute();
$stmt->bind_result($story_body);
$stmt->fetch();
$stmt->close();

$stmt = $mysqli->prepare("SELECT story_title FROM STORIES_LINK WHERE linkID=?");
if(!$stmt){
    printf("Query Prep Failed: %s\n", $mysqli->error);
    exit;
}
$stmt->bind_param('i', $id);
$stmt->execute();
$stmt->bind_result($story_title);
$stmt->fetch();
$stmt->close();
?>

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Module 3-Xueting&Shitianyu</title>

    <!-- Bootstrap Core CSS -->
    <link href="../bootstrap-3.3.6-dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../css/story_view.css" rel="stylesheet">
</head>

<body>

<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">HOME</a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li>
                    <a href="#">About</a>
                </li>
                <li>
                    <a href="#">Contact</a>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <?php
                if (is_null(SESSION('user_name'))) {
                    echo "<li><a href='login.php'>Sign in</a></li>";
                } else {
                    echo "<li><a href='#'>Hello, " . SESSION('user_name') . "</a></li>
                    <li><a href='logout.php'>Sign out</a></li>";
                }
                ?>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>
<div class="container">

    <div class="row">

        <!-- Blog Post Content Column -->
        <div class="col-lg-8">
            <div class="well">
                <form enctype="multipart/form-data" action="uploadStory.php"  method="post">
                    <h4>Title</h4>
                    <input type="text" name="linkID" value="<?php echo $id?>" hidden="hidden">
                    <input type="text" name="story_title" size="100" value="<?php echo $story_title?>">
                    <br>
                    <h4>Content</h4>
                    <div class="form-group">
                        <textarea class="form-control" rows="30" name="story_body"><?php echo $story_body?></textarea>
                    </div>
                    <label class="checkbox-inline"><input type="checkbox" name="art">Art</label>
                    <label class="checkbox-inline"><input type="checkbox" name="sport">Sport</label>
                    <label class="checkbox-inline"><input type="checkbox" name="tech">Technology</label>
                    <label class="checkbox-inline"><input type="checkbox" name="other">Other</label>
                    <br><br>
                    <p>
                        <input type="hidden" name="MAX_FILE_SIZE" value="20000000"/>
                        <label for="uploadimage_input">Choose an image to upload:</label>
                        <input name="image" type="file" id="uploadimage_input"/>
                    </p>
                    <br><br>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>

            <hr>
        </div>
    </div>
</div>
