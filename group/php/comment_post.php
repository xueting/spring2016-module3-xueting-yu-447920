<?php
/**
 * Created by PhpStorm.
 * User: Doublefinger
 * Date: 2/22/16
 * Time: 8:45 AM
 */

include 'util.php';
require 'database.php';
session_start();

//if user has already signed in, redirect to the file page
if (!isset($_SESSION['user_name'])) {
    header("Location: login.php");
    exit;
}

$comment_body = htmlentities(trim(POST('comment')));
$storyID = POST('storyID');
$author = POST('author');

$stmt = $mysqli->prepare("INSERT INTO COMMENTS (storyID, author, comment_body, created_at, updated_at) VALUES (?, ?, ?, now(), now())");
if (!$stmt) {
    printf("Query Prep Failed: %s\n", $mysqli->error);
    exit;
}
$stmt->bind_param('iss', $storyID, $author, $comment_body);
$stmt->execute();
$stmt->close();

header("Location: story_view.php/$storyID");
exit;