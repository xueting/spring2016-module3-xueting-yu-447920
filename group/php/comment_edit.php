<?php
/**
 * Created by PhpStorm.
 * User: Doublefinger
 * Date: 2/22/16
 * Time: 11:10 AM
 */
include 'util.php';
require 'database.php';
session_start();
$author = GET("comment_author");
$id = GET('commentID');

$username = SESSION('user_name');
if (is_null($username) || $username != $author) {
    header("Location: story_main.php");
    exit;
}

$stmt = $mysqli->prepare("SELECT comment_body FROM COMMENTS WHERE commentID=?");
if (!$stmt) {
    printf("Query Prep Failed: %s\n", $mysqli->error);
    exit;
}
$stmt->bind_param('i', $id);
$stmt->execute();
$stmt->bind_result($comment_body);
$stmt->fetch();
$stmt->close();

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">

    <title>Module 3-Xueting&Shitianyu</title>

    <!-- Bootstrap Core CSS -->
    <link href="../../bootstrap-3.3.6-dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../css/story_view.css" rel="stylesheet">
</head>

<body>
<form action="comment_update.php?commentID=<?php echo $id ?>" method="post">
    <input type="text" name="author" value="<?php echo $author ?>" hidden="hidden">
    <textarea class="form-control" rows="3" name="comment"></textarea>
    <button class="btn btn-default" type="submit">submit</button>
</form>
</body>
</html>